-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 21, 2023 at 10:25 AM
-- Server version: 8.0.31
-- PHP Version: 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spl`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutpage`
--

DROP TABLE IF EXISTS `aboutpage`;
CREATE TABLE IF NOT EXISTS `aboutpage` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `aboutpage`
--

INSERT INTO `aboutpage` (`id`, `title`, `body`) VALUES
(0, 'About us', 'Welcome to the heart and soul of football in the pristine hills of Meghalaya - the Shillong Premiere League (SPL). Founded in 2003, the SPL has evolved into one of the most prestigious and exciting football leagues in Northeast India, showcasing the region\'s love for the beautiful game.\n\n**Our Story**\n\nThe journey of the Shillong Premiere League began with a shared passion for football that united the communities of Shillong. Over the years, this league has grown in stature and significance, bringing together players, coaches, fans, and sponsors from across the region.\n\n**Our Vision**\n\nOur vision is simple yet profound: to promote football at the grassroots level, nurture local talent, and provide a platform for aspiring footballers to showcase their skills. We believe in the power of sport to unite people, bridge communities, and foster a sense of belonging.\n\n**The Essence of SPL**\n\n**Unity in Diversity**: The SPL reflects the rich cultural tapestry of Meghalaya. It transcends boundaries, bringing together people from various backgrounds, tribes, and walks of life under the common banner of football.\n\n**Passion for the Game**: Every matchday at the SPL is a celebration of football. The electric atmosphere in the stadiums, the resounding cheers of fans, and the fierce competition on the field are testaments to the unwavering passion for the game.\n\n**Talent Development**: We take pride in nurturing local talent and providing a pathway for young footballers to reach their full potential. Many players who have graced the SPL pitch have gone on to represent the state and even the national team.\n\n**Community Engagement**: Beyond the matches, the SPL is deeply involved in community outreach programs. We believe in giving back to society and using football as a tool for positive change.\n\n**Sustainability**: We are committed to making a positive impact on the environment. We promote sustainable practices within our league, stadiums, and among our stakeholders.\n\n**Join Us**\n\nWhether you\'re a football enthusiast, a budding player, or a dedicated sponsor, the Shillong Premiere League welcomes you with open arms. Together, we can continue to grow the sport, inspire the youth, and create unforgettable moments on the football field.\n\n**Contact Us**\n\nFor inquiries, partnerships, or any questions, please reach out to us at:\n\nEmail: info@shillongpremierleague.com\n\nPhone: +91-XXX-XXXXXX\n\nJoin us in celebrating the spirit of football and the vibrant culture of Meghalaya through the Shillong Premiere League. Together, we can continue to make history, one goal at a time.');

-- --------------------------------------------------------

--
-- Table structure for table `allteam`
--

DROP TABLE IF EXISTS `allteam`;
CREATE TABLE IF NOT EXISTS `allteam` (
  `id` int NOT NULL AUTO_INCREMENT,
  `teamName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `teamDescription` text NOT NULL,
  `teamLogo` varchar(255) NOT NULL,
  `teamCoach` varchar(255) NOT NULL,
  `coachDescription` text NOT NULL,
  `coachPhoto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gameplay`
--

DROP TABLE IF EXISTS `gameplay`;
CREATE TABLE IF NOT EXISTS `gameplay` (
  `id` int NOT NULL AUTO_INCREMENT,
  `teamA` int NOT NULL,
  `teamB` int NOT NULL,
  `score` varchar(10) DEFAULT NULL,
  `date` date NOT NULL,
  `time` varchar(12) NOT NULL,
  `venue` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
CREATE TABLE IF NOT EXISTS `place` (
  `id` int NOT NULL AUTO_INCREMENT,
  `venueName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `place`
--

INSERT INTO `place` (`id`, `venueName`) VALUES
(1, 'Wembley Stadium - London, England'),
(2, 'Camp Nou - Barcelona, Spain'),
(3, 'Santiago Bernabeu Stadium - Madrid, Spain'),
(5, 'Allianz Arena - Munich, Berlin'),
(6, 'San Siro (Giuseppe Meazza) - Milan, Italy'),
(7, 'Anfield - Liverpool, England'),
(8, 'Estadio Azteca - Mexico City, Mexico'),
(9, 'Maracanã Stadium - Rio de Janeiro, Brazil'),
(10, 'Emirates Stadium - London, England'),
(11, 'Signal Iduna Park (Westfalenstadion) - Dortmund, Germany'),
(12, 'Stamford Bridge - London, England'),
(13, 'Parc des Princes - Paris, France'),
(14, 'Estadio Monumental - Buenos Aires, Argentina'),
(15, 'MetLife Stadium - East Rutherford, New Jersey, USA');

-- --------------------------------------------------------

--
-- Table structure for table `point`
--

DROP TABLE IF EXISTS `point`;
CREATE TABLE IF NOT EXISTS `point` (
  `id` int NOT NULL AUTO_INCREMENT,
  `teamId` int NOT NULL,
  `matchPlay` int NOT NULL,
  `win` int NOT NULL,
  `draw` int NOT NULL,
  `lose` int NOT NULL,
  `goalFor` int NOT NULL,
  `goalAgainst` int NOT NULL,
  `points` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teamimage`
--

DROP TABLE IF EXISTS `teamimage`;
CREATE TABLE IF NOT EXISTS `teamimage` (
  `imageId` int NOT NULL AUTO_INCREMENT,
  `teamId` int NOT NULL,
  `teamPhoto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`imageId`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `teamimage`
--

INSERT INTO `teamimage` (`imageId`, `teamId`, `teamPhoto`) VALUES
(50, 53, '../images/teamImage/Aperiam laboriosam-Image-f305d26c-21d0-4d8b-85a7-563a05c22f06.jpeg'),
(42, 49, '../images/teamImage/Royal FC-Image-ff4bda44-acf2-4354-9d71-c307f6cc0713.jpeg'),
(41, 49, '../images/teamImage/Royal FC-Image-e39a42b7-555e-4e37-af69-ff5c3a761f68.jpeg'),
(40, 49, '../images/teamImage/Royal FC-Image-98e7c692-fff1-49cf-ac1e-d6aee87a932e.jpeg'),
(51, 53, '../images/teamImage/Aperiam laboriosam-Image-43b3c19f-03e4-4e75-9d8f-1d3fbb6de76f.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
CREATE TABLE IF NOT EXISTS `userrole` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
