<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "spl";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //set error mode to exception
        // echo "Connection Successful!";
    }   catch(PDOException $e) {
        echo "Connection Fails" .$e->getMessage();
    }
?>