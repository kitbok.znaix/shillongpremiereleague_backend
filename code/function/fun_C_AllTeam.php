<?php
    require('../code/connection.php');
    // require('../code/httpResponse.php');

    //-------------------CREATE------------------------//
    function createAllTeam($userInput) {
        global $conn; // global variable for db connection

        $teamName = empty($userInput['teamName']) ? httpResponse(422,'Enter the name of the Team') : validateString($userInput['teamName']);
        $teamDescription = empty($userInput['teamDescription']) ? httpResponse(422,'Enter Team Description') : validateString($userInput['teamDescription']);
        if($teamName){
            $teamLogo = empty($userInput['teamLogo']) ? httpResponse(422,'Upload Team Logo') : uploadFile($userInput['teamLogo'], '../images/teamLogo/', $teamName.'-Logo');
            }

        $teamCoach = empty($userInput['teamCoach']) ? httpResponse(422,'Enter Coach Team') : validateString($userInput['teamCoach']);
        $coachDescription = empty($userInput['coachDescription']) ? httpResponse(422,'Enter Coach Description') : validateString($userInput['coachDescription']);
        if($teamCoach){
            $coachPhoto = empty($userInput['coachPhoto']) ? httpResponse(422,'Upload Coach Photo') : uploadFile($userInput['coachPhoto'], '../images/coachPhoto/', $teamCoach.'-'.$teamName.'-');
            }
        // Check if teamName is not empty and teamLogo is empty
        if (!empty($teamName) && empty($userInput['teamLogo'])) {
            return httpResponse(422, 'Team Logo is required when Team Name is provided');
        } else if (!empty($teamLogo) && empty($userInput['teamName'])) {
            return httpResponse(422, 'Team Name is required when Team Logo is provided');
        }
        
        // One more Layer of Protection
        if ($teamName && $teamLogo && $teamDescription && $teamCoach && $coachDescription && $coachPhoto) {
                try {
                    $query = "INSERT INTO allteam (teamName, teamLogo, teamDescription, teamCoach, coachDescription, coachPhoto)
                    VALUES 
                    (:teamName, :teamLogo, :teamDescription, :teamCoach, :coachDescription, :coachPhoto)";

                    $stmt = $conn->prepare($query);
                    $stmt->bindParam(':teamName', $teamName);
                    $stmt->bindParam(':teamLogo', $teamLogo);
                    $stmt->bindParam(':teamDescription', $teamDescription);
                    $stmt->bindParam(':teamCoach', $teamCoach);
                    $stmt->bindParam(':coachDescription', $coachDescription);
                    $stmt->bindParam(':coachPhoto', $coachPhoto);

                    $result = $stmt->execute();

                    if ($result) {

                        $teamId = $conn->lastInsertId(); // Get the last inserted ID
                        //---------------- market -------------------//
                        if (!empty($userInput['teamPhoto'])) {
                            foreach ($userInput['teamPhoto'] as $index => $teamPhoto) {
                                $uploadteamPhoto = uploadFile($teamPhoto, '../images/teamImage/', $teamName . '-Image');
                        
                                $query = "INSERT INTO teamimage (teamId, teamPhoto) VALUES (:teamId, :teamPhoto)";
                                $stmtTeamImage = $conn->prepare($query);
                                $stmtTeamImage->bindParam(':teamId', $teamId);
                                $stmtTeamImage->bindParam(':teamPhoto', $uploadteamPhoto);
                        
                                $teamImageResult = $stmtTeamImage->execute();
                            }
                        
                            if ($teamImageResult) {
                                httpResponse(200, 'Team Image Uploaded Successfully');
                            } else {
                                httpResponse(500,'Error executing Image query: ' . implode(', ', $stmtTeamImage->errorInfo()));
                            }
                        } else {
                            httpResponse(206, 'No team Image provided');
                        }
                    } else {
                        httpResponse(500, 'Error executing Image query: ' . implode(', ', $stmt->errorInfo()));
                    }
                } catch (PDOException $e) {
                    httpResponse(500,'Database error: ' . $e->getMessage());
                }
            } else {
                // Handle the case where one or more variables are not valid
                httpResponse(422,'Please fill in all required fields with valid data');
            }
             
    }

?>
