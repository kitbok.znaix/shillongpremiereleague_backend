<?php
    require('../code/connection.php');
    

    //-------------------CREATE------------------------//
    function createGallery($userInput) {
        global $conn; // global variable for db connection
        if (!empty($userInput['photo'])) {
            foreach ($userInput['photo'] as $index => $photo) {
                $uploadPhoto = uploadFile($photo, '../images/gallery/', 'Gallery');
        
                if ($uploadPhoto) {
                    try {
                        $query = "INSERT INTO gallery (photo) VALUES (:photo)";
                        $stmt = $conn->prepare($query);
                        $stmt->bindParam(':photo', $uploadPhoto);
            
                        $galleryResult = $stmt->execute();
            
                        if ($galleryResult) {
                            httpResponse(201, 'Photo successfully added to gallery');
                        } else {
                            httpResponse(500,'Error executing query: ' . implode(', ', $stmt->errorInfo()));
                        }
                    } catch (PDOException $e) {
                        httpResponse(500,'Database error: ' . $e->getMessage());
                    }
                } else {
                    // Handle the case where file upload failed
                    httpResponse(422, 'Invalid image format');
                }
            }
        } else {
            httpResponse(422, 'Please Upload Image');
        }
    }
    

?>
