<?php
require('../code/connection.php');

//-------------------CREATE------------------------//
function createGamePlay($userInput)
{
    global $conn; // global variable for db connection

    $teamAName = empty($userInput['teamA']) ? httpResponse(422, 'Select the first team') : validateString($userInput['teamA']);
    $teamBName = empty($userInput['teamB']) ? httpResponse(422, 'Select the seond team') : validateString($userInput['teamB']);
    $score = empty($userInput['score']) ? httpResponse(422, 'Enter the Score') : validateScore($userInput['score']);
    $date = empty($userInput['date']) ? httpResponse(422, 'Enter the date to the match') : validateDate($userInput['date']);
    $time = empty($userInput['time']) ? httpResponse(422, 'Enter the Time of the match') : validateTime($userInput['time']);
    $venue = empty($userInput['venue']) ? httpResponse(422, 'Enter the venue') : validateString($userInput['venue']);

    // Get the IDs of the teams based on their names
    $venueId = isElementExists($venue,'place','venueName'); // search,table,column
    $teamAId = isElementExists($teamAName,'allteam','teamName');
    $teamBId = isElementExists($teamBName,'allteam','teamName');

    // Check if team names exist and their IDs are different
    if ($teamAId && $teamBId && $teamAId != $teamBId) {
        if ($score && $date && $time && $venueId) {
            try {
                $query = "INSERT INTO gameplay (teamA, teamB, score, date, time, venue)
                    VALUES 
                    (:teamA, :teamB, :score, :date, :time, :venue)";

                $stmt = $conn->prepare($query);
                $stmt->bindParam(':teamA', $teamAId);
                $stmt->bindParam(':teamB', $teamBId);
                $stmt->bindParam(':score', $score);
                $stmt->bindParam(':date', $date);
                $stmt->bindParam(':time', $time);
                $stmt->bindParam(':venue', $venueId);

                $result = $stmt->execute();

                if ($result) {
                    httpResponse(201, 'Gameplay added');
                } else {
                    httpResponse(500,'Error executing query: ' . implode(', ', $stmt->errorInfo()));
                }
            } catch (PDOException $e) {
                httpResponse(500, 'Database error: ' . $e->getMessage());
            }
        } else {
            // Handle cases where team names are not valid or IDs are the same
            httpResponse(422, 'Please fill in all required fields with valid data and ensure team names are different.');
        }
    } else {
        httpResponse(422, 'team A and team B cannot be the same');
    }
}

?>
