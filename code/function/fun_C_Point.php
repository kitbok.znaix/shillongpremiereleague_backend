<?php
    require('../code/connection.php');

    //-------------------CREATE------------------------//
    function createPoint($userInput) {
        global $conn; // global variable for db connection
    
        $teamName = empty($userInput['teamName']) ? httpResponse(422,'Enter Team Name') : validateString($userInput['teamName']);
        $matchPlay = !isset($userInput['matchPlay']) ? httpResponse(422, 'Enter Match Play') : validateNumber($userInput['matchPlay'], 'invalid MatchPlay');
        $win = !isset($userInput['win']) ? httpResponse(422, 'Enter Win') : validateNumber($userInput['win'], 'invalid Win');
        $draw = !isset($userInput['draw']) ? httpResponse(422, 'Enter Draw') : validateNumber($userInput['draw'], 'invalid Draw');
        $lose = !isset($userInput['lose']) ? httpResponse(422, 'Enter Lose') : validateNumber($userInput['lose'], 'invalid Lose');
        $goalFor = !isset($userInput['goalFor']) ? httpResponse(422, 'Enter Goal For') : validateNumber($userInput['goalFor'], 'invalid Goal For');
        $goalAgainst = !isset($userInput['goalAgainst']) ? httpResponse(422, 'Enter Goal Against') : validateNumber($userInput['goalAgainst'], 'invalid Goal Against');
        $points = !isset($userInput['points']) ? httpResponse(422, 'Enter Team Point') : validateNumber($userInput['points'], 'invalid Point');

    
        $teamIdCheck = isElementExists($teamName,'allteam','teamName');
        
        // Check if the teamId already exists in the points table
        if ($teamIdCheck) {
            $query = "SELECT teamId FROM point WHERE teamId = :teamId";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':teamId', $teamIdCheck);
            $stmt->execute();
        
            // Check if any rows were returned
            if ($stmt->rowCount() > 0) {
                httpResponse(400, 'Team point already created');
            }
            else {
                $teamId = $teamIdCheck;
            }
        }
        
        
        // Continue with the insertion
        if ($teamId && $matchPlay !== null && $win !== null && $draw !== null && $lose !== null && $goalFor !== null && $goalAgainst !== null && $points !== null) {
            try {
                // Prepare and execute the SQL query
                $query = "INSERT INTO point (teamId, matchPlay, win, draw, lose, goalFor, goalAgainst, points) VALUES (:teamId, :matchPlay, :win, :draw, :lose, :goalFor, :goalAgainst, :points)";
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':teamId', $teamId);
                $stmt->bindParam(':matchPlay', $matchPlay);
                $stmt->bindParam(':win', $win);
                $stmt->bindParam(':draw', $draw);
                $stmt->bindParam(':lose', $lose);
                $stmt->bindParam(':goalFor', $goalFor);
                $stmt->bindParam(':goalAgainst', $goalAgainst);
                $stmt->bindParam(':points', $points);
    
                $result = $stmt->execute();
    
                if ($result) {
                    httpResponse(200, 'Point created Successfully');
                } else {
                    httpResponse(500, 'Point creation failed');
                }
            } catch (PDOException $e) {
                httpResponse(500, 'Database error: ' . $e->getMessage());
            }
        } else {
            // Handle the case where one or more variables are not valid
            httpResponse(422, 'Please fill in all required fields with valid data');
        }
    }  

?>
