<?php
    require('../code/connection.php');

    //-------------------CREATE------------------------//
    function createUser($userInput) {
        global $conn; // global variable for db connection

        $userName = empty($userInput['userName']) ? httpResponse(422,'Enter the User Name') : validateString($userInput['userName']);
        $password = empty($userInput['password']) ? httpResponse(422,'Enter Password') : password_hash($userInput['password'], PASSWORD_BCRYPT);
        
        // One more Layer of Protection
        if ($userName && $password) {
            try {
                // Hash the password using the encryptPassword function
                // $hashedPassword = encryptPassword($password);
    
                // Prepare and execute the SQL query
                $query = "INSERT INTO userrole (userName, password) VALUES (:userName, :password)";
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':userName', $userName);
                $stmt->bindParam(':password', $password); // Use the hashed password
    
                $result = $stmt->execute();
    
                if ($result) {
                    httpResponse(201, 'User created successfully');
                } else {
                    herResponse(500, 'Fail to create user');
                }
            } catch (PDOException $e) {
                httpResponse(500, 'Database error: ' . $e->getMessage());
            }
        } else {
            // Handle the case where one or more variables are not valid
            httpResponse(422, 'Please fill in all required fields with valid data');
        }
    }

?>
