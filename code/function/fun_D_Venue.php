<?php
require('../code/connection.php');

//-------------------DELETE------------------------//
function deleteVenue($userInput) {
    global $conn; // global variable for db connection

    $venueId = empty($userInput['id']) ? httpResponse(422, 'Enter photo id') : validateNumber($userInput['id'], 'Invalid id');

    $venueIdCheck = isElementExists($venueId, 'place', 'id');

    if ($venueIdCheck) {

        // Check if $venueIdCheck exists in the 'gameplay' table's 'venue' column
        $query = "SELECT venue FROM gameplay WHERE venue = :venueIdCheck";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':venueIdCheck', $venueIdCheck);
        $stmt->execute();
        
        // Check if any rows were returned
        if ($stmt->rowCount() > 0) {
            httpResponse(400, 'Venue cannot be delete since it has already been fixed in match');
        }
        else {
            $venueIdDelete = $venueIdCheck;
        }

        if($venueIdDelete) {
            $queryDeleteVenue = "DELETE FROM place WHERE id=:venueIdDelete";
            $stmtDeleteVenue = $conn->prepare($queryDeleteVenue);
            $stmtDeleteVenue->bindParam(':venueIdDelete', $venueIdDelete);

            $result = $stmtDeleteVenue->execute();

            if ($result) {
            // Deletion from both the database and folder was successful
                httpResponse(200, 'Venue Successfully Deleted');
            } else {
            // Error occurred during database deletion
            httpResponse(500, 'Internal Server Error');
            }
        }
    
    } else {
        // Error occurred during file deletion
        httpResponse(422, 'Invalid id');
    } 
}
?>
