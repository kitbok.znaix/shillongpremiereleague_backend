<?php
require('../code/connection.php');

//-------------------READ------------------------//
function readAllTeam()
{
    global $conn; // global variable for db connection
// $query = "SELECT * FROM allteam";
$query = "SELECT * FROM allteam INNER JOIN teamimage ON allteam.id = teamimage.teamId;";


$stmt = $conn->prepare($query);
$stmt->execute();

if ($stmt) {
    if ($stmt->rowCount() > 0) {
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        // Create an associative array to group "teamPhoto" data by "id"
        $data = [
            'status' => '200',
            'message' => 'Successful',
            'data' => [],
        ];

        foreach ($result as $row) {
            $id = $row->id;

            // Check if the "id" already exists in the data array
            if (!array_key_exists($id, $data['data'])) {
                // If not, initialize it with the current data
                $data['data'][$id] = [
                    'id' => $id,
                    'teamName' => $row->teamName,
                    'teamDescription' => $row->teamDescription,
                    'teamLogo' => $row->teamLogo,
                    'teamCoach' => $row->teamCoach,
                    'coachDescription' => $row->coachDescription,
                    'coachPhoto' => $row->coachPhoto,
                    'teamId' => $row->teamId,
                    'teamPhoto' => [],
                ];
            }

            // Add the "teamPhoto" data to the existing "teamPhoto" array
            $data['data'][$id]['teamPhoto'][] = [
                'imageId' => $row->imageId,
                'imageSrc' => $row->teamPhoto,
            ];
        }

        // Convert the associative array to a simple indexed array
        $data['data'] = array_values($data['data']);

        // header('Content-Type: application/json');
        return json_encode($data);
            
            
            // return json_encode($data);
                // httpResponse(200, 'Fetch Successful'); // Set the HTTP response code to 200
        } else {
            // $data = [
            //     'status' => '404',
            //     'message' => 'No results!'
            // ];
            // return json_encode($data);
            httpResponse(404, 'No results Found!'); // Set the HTTP response code to 422
        }
    } else {
        // $data = [
        //     'status' => '500',
        //     'message' => 'Error executing query'
        // ];
        // return json_encode($data);
        httpResponse(500, 'Error executing query'); // Set the HTTP response code to 500
    }
}


?>
