<?php
    require('../code/connection.php');

//-------------------READ------------------------//
    function readDeleteAllTeam() {
        global $conn; // global variable for db connection

        // Query to retrieve teams from allteam that do not exist in gameplay
        $query = "SELECT id, teamName FROM allteam 
                WHERE id NOT IN (SELECT teamA FROM gameplay UNION SELECT teamB FROM gameplay)";
        
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $teams = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Check if there are teams to display
        if (empty($teams)) {
            httpResponse(404, 'No teams found that can be deleted');
        } else {
            // Initialize an array to store the team data
            $data = [];

            // Loop through the teams and add them to the data array
            foreach ($teams as $team) {
                $data[] = [
                    'id' => $team['id'],
                    'teamName' => $team['teamName']
                ];
            }

            $deleteData = [
                'status' => '200',
                'message' => 'Delete any of these team will also delete all associative element with it!',
                'data' => $data
            ];
            // Return the JSON-encoded data
            return json_encode($deleteData);
        }
    }


?>
