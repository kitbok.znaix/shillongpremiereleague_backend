<?php
require('../code/connection.php');

//-------------------READ------------------------// OLD ARRAY DISPLAY
function readGamePlay()
{
    global $conn; // global variable for db connection
    $query = "SELECT
                gameplay.id,
                gameplay.score,
                gameplay.date,
                gameplay.time,
                place.venueName AS venue,
                teamA.teamName AS teamAName,
                teamB.teamName AS teamBName,
                teamA.teamLogo AS teamALogo,
                teamB.teamLogo AS teamBLogo
            FROM
                gameplay
            LEFT JOIN
                allteam AS teamA ON gameplay.teamA = teamA.id
            LEFT JOIN
                allteam AS teamB ON gameplay.teamB = teamB.id
            LEFT JOIN
                place ON gameplay.venue = place.id;

    ";

$stmt = $conn->prepare($query);
$stmt->execute();

    if ($stmt) {
        if ($stmt->rowCount() > 0) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $data = [
                'status' => '200',
                'message' => 'Successful',
                'data' => $result
            ];
            return json_encode($data);
        } else {
            httpResponse(404, 'No results Found!'); // Set the HTTP response code to 422
        }
    } else {
        httpResponse(500, 'Error executing query'); // Set the HTTP response code to 500
    }
}


?>
