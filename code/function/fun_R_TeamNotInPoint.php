<?php
    require('../code/connection.php');

    //------READ Team Not in POINT Table-----------//
    function readAllTeamNotInPoint()
{
    global $conn; // global variable for db connection

    try {
        // Use a subquery to select the teamIds from the point table
        $subquery = "SELECT DISTINCT teamId FROM point";
        
        // Select all teams from allteam that are not in the point table
        $query = "SELECT teamName FROM allteam WHERE id NOT IN ($subquery)";
        
        $stmt = $conn->prepare($query);
        $stmt->execute();

        if ($stmt) {
            if ($stmt->rowCount() > 0) {
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $data = [
                    'status' => '200',
                    'message' => 'Successful',
                    'data' => $result
                ];
                return json_encode($data);
            } else {
                httpResponse(404, 'No results Found!');
            }
        } else {
            httpResponse(500, 'Error executing query');
        }
    } catch (PDOException $e) {
        httpResponse(500, 'Database error: ' . $e->getMessage());
    }
}


?>
