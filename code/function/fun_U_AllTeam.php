<?php
    require('../code/connection.php');

    //-------------------CREATE------------------------//
    function updateAllTeam($userInput) {
        global $conn; // global variable for db connection

        $teamIdCheck = empty($userInput['id']) ? httpResponse(422,'Enter team id') : validateNumber($userInput['id'], 'Invalid id');
        if($teamIdCheck) {
            $teamId = isElementExists($teamIdCheck,'allteam','id');
        }

        $teamName = empty($userInput['teamName']) ? httpResponse(422,'Enter team teamName') : validateString($userInput['teamName']);
        $teamCoach = empty($userInput['teamCoach']) ? httpResponse(422,'Enter team teamCoach') : validateString($userInput['teamCoach']);

        if($teamId && $teamName && $teamCoach) {            
            $updates = []; // Initialize an empty array to store the columns and values to be updated

            $updates[] = "teamName = :teamName";
            $updates[] = "teamCoach = :teamCoach";

            // Check and update the Specific field if provided
            if (!empty($userInput['teamDescription'])) {
                $teamDescription = validateString($userInput['teamDescription']);
                $updates[] = "teamDescription = :teamDescription";
            } 
            
            if(!empty($userInput['coachDescription'])) {
                $coachDescription = validateString($userInput['coachDescription']);
                $updates[] = "coachDescription = :coachDescription";
            }

            if(!empty($userInput['teamLogo'])) {
                // Get the image from database
                $queryTeamLogo = "SELECT teamLogo FROM allteam WHERE id = :teamId";
                $stmtTeamLogo = $conn->prepare($queryTeamLogo);
                $stmtTeamLogo->bindParam(':teamId', $teamId);
                $stmtTeamLogo->execute();
                $currentImages = $stmtTeamLogo->fetch(PDO::FETCH_ASSOC);

                if (!empty($currentImages['teamLogo'])) {
                    // Upload first then Delete
                    $teamLogo = uploadFile($userInput['teamLogo'], '../images/teamLogo/', $teamName.'-Logo');

                    if ($teamLogo) {
                        $deletedTeamLogo = deleteImage($currentImages['teamLogo']);
                        if ($deletedTeamLogo) {
                            $updates[] = "teamLogo = :teamLogo";
                        } else {
                            httpResponse(400, 'Team Logo is not deleted. Please try again');
                        }

                    } else {
                        httpResponse(400, 'Team Logo is not uploaded. Please upload a valid base64 format');
                    }
                } else {
                    httpResponse(422, 'The image id not found on database');
                } 
            }

            if (!empty($userInput['coachPhoto'])) {
                // Get the coachPhoto image from the database
                $queryCoachPhoto = "SELECT coachPhoto FROM allteam WHERE id = :teamId";
                $stmtCoachPhoto = $conn->prepare($queryCoachPhoto);
                $stmtCoachPhoto->bindParam(':teamId', $teamId);
                $stmtCoachPhoto->execute();
                $currentImages = $stmtCoachPhoto->fetch(PDO::FETCH_ASSOC);
            
                if (!empty($currentImages['coachPhoto'])) {
                    // Upload the new coachPhoto image
                    $coachPhoto = uploadFile($userInput['coachPhoto'], '../images/coachPhoto/', $teamCoach.'-'.$teamName.'-');
            
                    if ($coachPhoto) {
                        // Delete the old coachPhoto image from the server
                        $deletedCoachPhoto = deleteImage($currentImages['coachPhoto']);
            
                        if ($deletedCoachPhoto) {
                            $updates[] = "coachPhoto = :coachPhoto";
                        } else {
                            httpResponse(400, 'Coach Photo is not deleted. Please try again');
                        }
                    } else {
                        httpResponse(400, 'Coach Photo is not uploaded');
                    }
                } else {
                    httpResponse(422, 'The coachPhoto is not found in the database');
                }
            }            

                    try {
                        // Build the SQL UPDATE query dynamically
                        $query = "UPDATE allteam SET " . implode(', ', $updates) . " WHERE id = :teamId";

                        $stmt = $conn->prepare($query);

                        $stmt->bindParam(':teamId', $teamId);
                        $stmt->bindParam(':teamName', $teamName);
                        $stmt->bindParam(':teamCoach', $teamCoach);
                
                        if (!empty($userInput['teamDescription'])) {
                            $stmt->bindParam(':teamDescription', $teamDescription);
                        }

                        if (!empty($userInput['coachDescription'])) {
                            $stmt->bindParam(':coachDescription', $coachDescription);
                        }

                        if (!empty($userInput['teamLogo'])) {
                            $stmt->bindParam(':teamLogo', $teamLogo);
                        }

                        if (!empty($userInput['coachPhoto'])) {
                            $stmt->bindParam(':coachPhoto', $coachPhoto);
                        }
                        

                        $result = $stmt->execute();

                        if ($result) {
                            //---------------- TeamImage-------------------//
                            if (isset($userInput['teamPhoto']) && !empty($userInput['teamPhoto']) && is_array($userInput['teamPhoto'])) {
                                foreach ($userInput['teamPhoto'] as $index => $teamPhoto) {
                                    if ($teamPhoto !== null) {
                                        $imageId = $teamPhoto['imageId'];
                            
                                        // Get the current teamPhoto path from the database
                                        $queryCurrentTeamPhoto = "SELECT teamPhoto FROM teamimage WHERE teamId = :teamId AND imageId = :imageId";
                                        $stmtCurrentTeamPhoto = $conn->prepare($queryCurrentTeamPhoto);
                                        $stmtCurrentTeamPhoto->bindParam(':teamId', $teamId);
                                        $stmtCurrentTeamPhoto->bindParam(':imageId', $imageId);
                                        $stmtCurrentTeamPhoto->execute();
                                        $currentTeamPhoto = $stmtCurrentTeamPhoto->fetch(PDO::FETCH_ASSOC);
                            
                                        if (!empty($currentTeamPhoto['teamPhoto'])) {
                                            // Upload the new teamPhoto image
                                            $uploadTeamPhoto = uploadFile($teamPhoto['imageSrc'], '../images/teamImage/', $teamName . '-' . $imageId . '-Image');
                            
                                            if ($uploadTeamPhoto) {
                                                // Delete the old teamPhoto image from the server
                                                $deletedTeamPhoto = deleteImage($currentTeamPhoto['teamPhoto']);
                            
                                                if ($deletedTeamPhoto) {
                                                    // Update the teamimage table with the new path
                                                    $queryUpdateTeamPhoto = "UPDATE teamimage
                                                                            SET teamPhoto = :teamPhoto
                                                                            WHERE teamId = :teamId AND imageId = :imageId";
                                                    $stmtUpdateTeamPhoto = $conn->prepare($queryUpdateTeamPhoto);
                                                    $stmtUpdateTeamPhoto->bindParam(':teamPhoto', $uploadTeamPhoto);
                                                    $stmtUpdateTeamPhoto->bindParam(':teamId', $teamId);
                                                    $stmtUpdateTeamPhoto->bindParam(':imageId', $imageId);
                                                    $teamImageResult = $stmtUpdateTeamPhoto->execute();
                            
                                                    if (!$teamImageResult) {
                                                        httpResponse(500, 'Error executing Team Image query: ' . implode(', ', $stmtUpdateTeamPhoto->errorInfo()));
                                                    }
                                                } else {
                                                    httpResponse(400, 'Team Photo is not deleted. Please try again');
                                                }
                                            } else {
                                                httpResponse(400, 'Team Photo is not uploaded. Please upload a valid image');
                                            }
                                        } else {
                                            httpResponse(422, 'The teamPhoto with imageId ' . $imageId . ' was not found in the database');
                                        }
                                    }
                                }
                            
                                // Check if all teamPhoto updates were successful
                                if ($teamImageResult) {
                                    httpResponse(200, 'Team updated successfully');
                                } elseif ($result && !$teamImageResult) {
                                    httpResponse(206, 'No team images updated');
                                }
                            } else {
                                httpResponse(206, 'No team images provided for update');
                            }
                            
                            

                        } else {
                            httpResponse(500, 'Error executing All Team query: ' . implode(', ', $stmt->errorInfo()));
                            }      
                        
                    } catch (PDOException $e) {
                        return httpResponse(500,'Database error: ' . $e->getMessage());
                    }
                
                
        } else {
            httpResponse(422, 'id, team name and team coach is required');
        }
}


?>
