<?php
    require('../code/connection.php');

    //-------------------UPDATE------------------------//
    function updatePoint($userInput) {
        global $conn; // global variable for db connection

        $teamIdGet = empty($userInput['teamId']) ? httpResponse(422,'Enter Team id') : validateNumber($userInput['teamId'],'invalid id');

        $teamId = isElementExists($teamIdGet,'point','teamId');

        $updates = []; // Initialize an empty array to store the columns and values to be updated

        if (isset($userInput['matchPlay'])) {
            $matchPlay = validateNumber($userInput['matchPlay'], 'invalid matchplay');
            $updates[] = "matchPlay = :matchPlay";
        }

        if (isset($userInput['win'])) {
            $win = validateNumber($userInput['win'], 'invalid win');
            $updates[] = "win = :win";
        }

        if (isset($userInput['draw'])) {
            $draw = validateNumber($userInput['draw'], 'invalid draw');
            $updates[] = "draw = :draw";
        }

        if (isset($userInput['lose'])) {
            $lose = validateNumber($userInput['lose'], 'invalid lose');
            $updates[] = "lose = :lose";
        }

        if (isset($userInput['goalFor'])) {
            $goalFor = validateNumber($userInput['goalFor'], 'invalid goalFor');
            $updates[] = "goalFor = :goalFor";
        }

        if (isset($userInput['goalAgainst'])) {
            $goalAgainst = validateNumber($userInput['goalAgainst'], 'invalid goalAgainst');
            $updates[] = "goalAgainst = :goalAgainst";
        }

        if (isset($userInput['points'])) {
            $points = validateNumber($userInput['points'], 'invalid points');
            $updates[] = "points = :points";;
        }

        // Continue with the insertion
        if ($teamId && !empty($updates)) {
            try {
                // Build the SQL UPDATE query dynamically
                $query = "UPDATE point SET " . implode(', ', $updates) . " WHERE id = :teamId";
                // echo "SQL Query: $query";
                // echo "TeamID: $teamId";

                $stmt = $conn->prepare($query);
                $stmt->bindParam(':teamId', $teamId);

                if (isset($matchPlay)) {
                    $stmt->bindParam(':matchPlay', $matchPlay);
                }
                if (isset($win)) {
                    $stmt->bindParam(':win', $win);
                }
                if (isset($draw)) {
                    $stmt->bindParam(':draw', $draw);
                }
                if (isset($lose)) {
                    $stmt->bindParam(':lose', $lose);
                }
                if (isset($goalFor)) {
                    $stmt->bindParam(':goalFor', $goalFor);
                }
                if (isset($goalAgainst)) {
                    $stmt->bindParam(':goalAgainst', $goalAgainst);
                }
                if (isset($points)) {
                    $stmt->bindParam(':points', $points);
                }

                $result = $stmt->execute();

                if ($result) {
                    httpResponse(200, 'Point information updated successfully');
                } else {
                    httpResponse(500, 'Error executing query: ' . implode(', ', $stmt->errorInfo()));
                }
            } catch (PDOException $e) {
                httpResponse(500, 'Database error: ' . $e->getMessage());
            }
        } else {
            httpResponse(422, 'id and at least one field to update are required');
        }
    }
?>
