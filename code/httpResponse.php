<?php

function httpResponse($status, $message) {
    // header('Content-Type: application/json');
    $data = [
        'status' => $status,
        'message' => $message,
    ];
    http_response_code($status);
    echo json_encode($data);
    exit();
}

?>
