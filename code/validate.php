<?php

    function uploadFile($base64ImageData, $uploadDir, $fileName) {
        if (is_string($base64ImageData)) {
            if (strpos($base64ImageData, ';') !== false) {
                list($mime, $base64Data) = explode(';', $base64ImageData);
                list(, $mime) = explode(':', $mime);
                $extension = explode('/', $mime)[1];
                $pureBase64 = explode(',', $base64Data)[1];
                $imageData = base64_decode($pureBase64);
    
                $uuid = generateUUIDv4();
    
                $filePath = $uploadDir . $fileName . '-' . $uuid . '.' . $extension;
    
                if (file_put_contents($filePath, $imageData)) {
                    return $filePath;
                } else {
                    httpResponse(422, 'Invalid image format' . '-' . $fileName); // File saving failed
                }
            } else {
                // Handle the case where $base64ImageData doesn't contain a semicolon
                httpResponse(422, 'Invalid base64 format' . '-' . $fileName);
            }
        } else {
            httpResponse(422, 'Base64 must be a string' . '-' . $fileName);
        }
    }
    
    function generateUUIDv4() {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // Version 4
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // Variant
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    


    function validateString($inputString) {
        // Trim the input string
        $trimmedString = trim($inputString);
    
        // Sanitize the trimmed string using FILTER_SANITIZE_FULL_SPECIAL_CHARS
        $sanitizedString = filter_var($trimmedString, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    
        return $sanitizedString;
    }
    

    function validateNumber($number, $errorMessage) {
        $validatedNumber = filter_var($number, FILTER_VALIDATE_INT);
        if ($validatedNumber !== false) {
            $sanitizedNumber = filter_var($validatedNumber, FILTER_SANITIZE_NUMBER_INT);
            return $sanitizedNumber;
        } else {
            httpResponse(422, $errorMessage);
        }
    }
    

    // function isTeamIdExists($id,$table,$column) {
    //     global $conn; // global variable for db connection
    
    //     // Prepare a SELECT query to check if the team ID exists
    //     $query = "SELECT id FROM $table WHERE id = :id";
    //     $stmt = $conn->prepare($query);
    //     $stmt->bindParam(':id', $id);
    //     $stmt->execute();
    
    //     // Fetch the result as an associative array
    //     $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
    //     // Check if the result is not empty, indicating that the ID exists
    //     if (!empty($result)) {
    //         return $id; // ID exists
    //     } else {
    //         httpResponse(422,'team id is not on the database'); // ID does not exist
    //     }
    // }

    // Function to get team ID by name from the table
    function isElementExists($search,$table,$column)
    {
        global $conn; // global variable for db connection

        try {
            $query = "SELECT id FROM $table WHERE $column = :search";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(':search', $search);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($result) {
                return $result['id'];
            } else {
                httpResponse(422, 'Not found on database'); // Team name not found
            }
        } catch (PDOException $e) {
            httpResponse(500, 'Database error: ' . $e->getMessage());
        }
    }

    function deleteImage($filePaths) {
        try {
            if (is_array($filePaths)) {
                $deletedAll = true;
                foreach ($filePaths as $filePath) {
                    if (file_exists($filePath)) {
                        if (!unlink($filePath)) {
                            $deletedAll = false;
                        }
                    } else {
                        $deletedAll = false;
                    }
                }
                if ($deletedAll) {
                    return true; // All images deleted successfully
                } else {
                    httpResponse(500, 'Error deleting one or more images');
                }
            } elseif (is_string($filePaths)) {
                if (file_exists($filePaths)) {
                    if (unlink($filePaths)) {
                        return true; // Image deleted successfully
                    } else {
                        httpResponse(500, 'Error deleting image');
                    }
                } else {
                    httpResponse(500, 'Image not found: ' . $filePaths);
                }
            } else {
                httpResponse(500, 'Invalid input: ' . var_export($filePaths, true));
            }
        } catch (PDOException $e) {
            httpResponse(500, 'Database error: ' . $e->getMessage());
            return false; // Error occurred
        }
    }
    


    function validateScore($score)
    {
        // Define the pattern for a score in the format "number - number"
        $pattern = '/^(\d+\s?-\s?\d+|Not yet)$/i';

        if (preg_match($pattern, $score) === 1) {
            return $score;
        } else {
            httpResponse(422, 'Invalid Score format. Use (number - number) Or Not yet');
        }
    }


    function validateDate($date)
    {
        // $dateObj = DateTime::createFromFormat('d-m-Y', $date);

        // if ($dateObj && $dateObj->format('d-m-Y') === $date) {
        //     return $date; // Valid date
        // } else {
        //     httpResponse(422,'invalid date format. Use(d-m-Y)'); // Invalid date
        // }
        return $date;
    }
    

    function validateTime($time)
    {
        // Define the pattern for a 12-hour time format with AM/PM
        // $pattern = '/^(0[1-9]|1[0-2]):[0-5][0-9]:[0-5][0-9] (AM|PM)$/i';

        // if(preg_match($pattern, $time) === 1) {
        //     return $time;
        // } else {
        //     httpResponse(422,'invalid Time format. Use(HH:mm:ss AM/PM)');
        // }

        return $time;
    }


    function encryptPassword($password) {
        // Generate a random salt
        $salt = base64_encode(random_bytes(16));
    
        // Create a password hash with bcrypt algorithm and the generated salt
        $hashedPassword = password_hash($password, PASSWORD_BCRYPT, ['salt' => $salt]);
    
        return $hashedPassword;
    }
?>