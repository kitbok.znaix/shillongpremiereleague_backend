<?php

include('../code/httpResponse.php');
include('../code/validate.php');
include('../code/token.php');

function createAll($handleCreate) {

     // Allow the preflight request
     header("Access-Control-Allow-Credentials: true");
     header("Access-Control-Allow-Origin: *"); // You can replace * with specific origins
     header("Access-Control-Allow-Methods: POST, OPTIONS");
     header("Access-Control-Allow-Headers: Content-Type, Authorization");

    $requestMethod = $_SERVER['REQUEST_METHOD'];

    if ($requestMethod === 'OPTIONS') {
        // Allow the preflight request
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Origin: *"); // You can replace * with specific origins
        header("Access-Control-Allow-Methods: POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        exit();
    }

    $token = getToken();
    $tokenValid = verifyToken($token);
    // echo $tokenValid;

    // Check if $tokenValid is a JSON string and decode it into an array
    if (is_string($tokenValid)) {
        $tokenValidDecode = json_decode($tokenValid, true);
    }

    // Check if $tokenValidDecode is an array and has a 'status' key
    if (isset($tokenValidDecode['status']) && $tokenValidDecode['status'] === 200) {
            if ($requestMethod === 'POST') {
                // Check if Content-Type is application/json
                $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
                if ($contentType === 'application/json') {
                    // Handle JSON input
                    $inputData = json_decode(file_get_contents("php://input"), true);
                } else {
                    // Handle form data input
                    $inputData = $_POST;
                }
                $storeRecord = $handleCreate($inputData);
                return [
                    'storeRecord' => $storeRecord,
                    'tokenValid' => $tokenValidDecode,
                ];
            } else {
                // Handle invalid request method
                $data = [
                    'status' => '405',
                    'message' => $requestMethod . ' Method Not Allowed'
                ];
                http_response_code(405); // Set the HTTP response code to 405
                return json_encode($data);
            }
    } else {
        // Handle the case where the condition is false
        http_response_code(422);
        echo $tokenValid;
    }
}

?>
